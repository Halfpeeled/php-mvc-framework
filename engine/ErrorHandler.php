<?php
/**
 * WIP ErrorNav and Exception handling class
 *
 * TODO Have this write errors to the internal app database
 */

/**
 * @changelog
 *
 */

namespace util\engine;

use PDOException;

class ErrorHandler
{
    public static $errorDB;

    /**
     * ErrorHandler constructor.
     */
    public function __construct()
    {
        register_shutdown_function ("util\\engine\\ErrorHandler::handleFatal");
        self::$errorDB = $this->initErrorDatabase();
    }

    /**
     *  Initiate ErrorNav handler database
     */
    public function initErrorDatabase()
    {
        try
        {
            // TODO Might need to make host environmentally aware /dynamic (localhost vs remote host...), not sure yet...
            // TODO Also make the configuration cleaner
            // TODO Also figure out if there is a better way to get the relative path for the ErrorNav database from the public root execution point

            // Define the PDO configurations
            $config = ["driver" => "sqlite", "database" => "../util/database/errors.sqlite"];

            // Instantiate the ErrorNav Handler database
            $errorDB = new DatabaseEngine($config, true);

            // Construct the ERRORS table configurations array
            $tableConfig = ["table" => "ERRORS",
                "columns" => array("error_id INTEGER PRIMARY KEY AUTOINCREMENT", "app_error_text TEXT", "php_error_text TEXT", "error_date DATE")];

            // Create the ERRORS table using the configurations array
            $errorDB->createSQLiteTable($tableConfig);
            return $errorDB;
        }
        catch(PDOException $e)
        {
            $error = $e->getMessage();
            echo "<script>alert('Fatal Exception encountered attempting to initiate ErrorHandler Database: $error');</script>";
        }
    }

    /**
     * Handles fatal exit conditions for the application.
     *
     * TODO Currently just printing the last ErrorNav, needs to be heavily refined, and ultimately write a log file.
     */
    public static function handleFatal()
    {
        if(isset(error_get_last()["message"]))
        {
            $appError = "<pre> Fatal ErrorNav (" . error_get_last()["line"] . "): " . error_get_last()["message"]
                . (isset(error_get_last()["file"]) ? error_get_last()["file"] : "") . "</pre>";
            print_r($appError, true);
            self::logError($appError, "");
        }
    }

    /**
     * Logs the passed application and PHP ErrorNav in the embedded ErrorNav database
     * @param string $appError The application specific ErrorNav
     * @param string $phpError The PHP specific ErrorNav
     */
    public static function logError($appError)
    {
        $phpError = error_get_last()["message"];

        self::$errorDB->singleInsert("ERRORS", array("app_error_text", "php_error_text", "error_date"),
            array($appError, $phpError, date("Y-m-d @H:i.s")));
    }

    /**
     * Returns an associative array of all ErrorNav ErrorNav
     * @return array
     */
    public static function getAllLogs()
    {
        return self::$errorDB->resultSet("SELECT * FROM ERRORS;");
    }

    /**
     * Returns the last logged ErrorNav as an associative array
     * @return array
     */
    public static function getLastLog()
    {
        return self::$errorDB->singleSet("ERRORS", ["error_id", "app_error_text", "php_error_text", "error_date"],
            "WHERE error_date = (SELECT MAX(error_date) FROM ERRORS)");;
    }

    public function getLogRange($upper, $lower)
    {

    }
}