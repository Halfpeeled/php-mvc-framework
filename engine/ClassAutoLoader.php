<?php
/**
 * This automatically loads all of the necessary class files, alleviating the need for require statements.
 *
 * The static autoLoad method takes a class name and the application's root directory as parameters, and is called from
 * the application's front-controller 'index.php' located within the applications **public root**.
 *
 * The method is registered as a callback function by PHP's built-in spl_autoload_register, which is the preferred way to
 * auto load class files, as __autoload can conflict with vendor auto loading.
 *
 * The callback function is triggered every time an object of a class is instantiated without the appropriate include or require statement.
 * This is triggered both explicitly within the front-controller, e.g., like instantiating the RoutingEngine, and by
 * dynamic instantiations via route dispatching.
 *
 * When the function is called, it passes the name of the class (including namespace), and the applications root directory to this in-house method
 * to facilitate finding all contained class files. The namespace aids in constructing the directories of the class files
 * by converting the separators.
 */

namespace util\engine;

class ClassAutoLoader
{
    /**
     * Auto loads all of the classes by reconstructing the pass class name and application root directory in to a
     * directory stirng and then requiring it.
     * @param string $class The class name
     * @param string $root The application root directory
     * @return bool
     */
    public static function autoLoad($class, $root)
    {
        // Convert the namespace\ClassName to a directory by sanitizing the string of all backslashes, and then suffixing with the php file extension
        $file = $root . '/' . str_replace("\\", "/", $class) . ".php";

        // If the file exists
        if(is_readable($file))
        {
            // Require it
            require $root . '/' . str_replace("\\", "/", $class) . ".php";
            return true;
        }
        else
        {
            echo "<pre>No matching class file for { $file } found for class {$class}</pre>";
        }
        return false;
    }
}