<?php
/**
 * Created by PhpStorm.
 * User: Halfpeeled
 * Date: 3/5/2018
 * Time: 7:51 PM
 */

namespace util\engine;


class Engine
{
    public static $errorHandler;
    protected $router;

    /**
     * Engine constructor.
     */
    public function __construct()
    {
        /** @var $errorHandler */
        self::$errorHandler = new ErrorHandler(); // Instantiate the ErrorNav handler

        /** @var $router */
        $this->router = new RoutingEngine(); // Instantiate the routing engine
    }

    public function start()
    {
        // Handles landing page route requests that are either just the domain, or the domain followed by index
        $this->router->addRoute('(index)?', ['controller' => 'Home', 'action' => 'index']);

        // Generic variable controller and variable action within the root controller namespace/directory
        $this->router->addRoute('{controller}/{action}');

        // Match any route with a variable controller, variable id, and variable action, in the Dashboard namespace/subdirectory
        $this->router->addRoute('{controller}/{id:\d+}/{action}', ['namespace' => 'dashboard']);
        $this->router->addRoute('{controller}/{id:\d+}/', ['namespace' => 'dashboard', 'action' => 'index']);

        // ErrorNav Navigator index
        $this->router->addRoute('{controller}(/)?', ['namespace' => 'errorNav', 'action' => 'index']);
        $this->router->addRoute('{controller}/{action}', ['namespace' => 'errorNav']);

        $this->router->dispatch($_SERVER['QUERY_STRING']); // Dispatch the route request to the appropriate controller
    }

    /**
     * @param $mode
     */
    public function debug($mode)
    {
        switch ($mode)
        {
            case "route" :
                $url = $_SERVER['QUERY_STRING'];
                echo "<pre>Requested URL " . $url . ($this->router->matchRoute($url) ? "" : " not") . " found.</pre>";
                break;
            default :
                var_dump(self::$errorHandler->getAllLogs());
        }
    }

    /**
     * @return ErrorHandler
     */
    public function getErrorHandler(): ErrorHandler
    {
        return self::$errorHandler;
    }
}