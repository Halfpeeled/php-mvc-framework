<?php
/**
 * This class serves as the applications base controller that all of the other controllers inherit from.
 */

namespace util\engine;


abstract class Controller
{
    /**
     * Parameters from the matched route
     * @var array
     */
    protected $routeParams = [];

    /**
     * Controller constructor.
     * @param $routeParams
     */
    public function __construct($routeParams)
    {
        $this->routeParams = $routeParams;
    }

    /**
     * This magic method is triggered when invoking inaccessible methods in an object context
     * @param string $name The name of the method being called
     * @param array $arguments The arguments of the method call
     */
    public function __call($name, $arguments)
    {
        $method = $name . "_Action"; // Concatenate the incoming action name with the common suffix

        // If the method exist for this class
        if(method_exists($this, $method))
        {
            // Entrance filter for action method calls
            $this->beforeCall($method, $arguments);

            // Invoke callback method with an array of parameters and returns either the value of the successfully called method, or false if failed
           call_user_func_array([$this, $method], $arguments);

            // Exit filter for action method calls
            $this->afterCall($method, $arguments);
        }
        else
        {
            echo "<pre>Method { $method } not found for Controller class { " . get_class($this) . " }</pre>";
        }
    }

    /**
     * This method is executed before the controller request is resolved
     * acting as a filter for action requests, such as validation, etc.
     * @param string $method
     * @param array $arguments
     */
    protected function beforeCall($method, $arguments)
    {

    }

    /**
     * This method is executed after the controller request is resolved
     * acting as a filter for action results, such as validation, etc.
     * @param string $method
     * @param array $arguments
     */
    protected function afterCall($method, $arguments)
    {

    }
}
