<?php
/**
 * This class renders views
 */

namespace util\engine;


class View
{
    /**
     * Renders views by simply matching the passed view name against files
     * in the 'views' directory and then requiring them. The view runs
     * automatically following the require as there is no further execution.
     * @param string $view The name of the view file to be rendered
     * @param string $template
     */
    public static function renderView($view)
    {
        $viewFile = "../app/views/$view";

        if(is_readable($viewFile))
        {
            require $viewFile;
        }
        else
        {
            echo "View { $viewFile } not found";
        }
    }
}