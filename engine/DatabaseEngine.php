<?php
/**
 * WIP Class for handling Database connections and transactions
 */

namespace util\engine;

use PDO;
use PDOException;

class DatabaseEngine
{
    // Database Handler
    private $databaseHandler;

    // SQL Statements
    private $sqlStatement;

    /**
     * Parameter that determines if connection should remain persistent on instantiation.
     * By default this is set to false, and means that the connection will be closed once the script has completed execution.
     */
    private $persist;

    /**
     * DatabaseEngine constructor.
     * @param $config
     * @param $internal
     */
    public function __construct($config, $internal)
    {
        // Construct the database source name
        // If internal is set to true, then this is a SQLLite $configuration, so ignore the other arguments
        if($internal == true)
        {
            $dsn = $config["driver"] . ":" . $config["database"];

            // Create PDO instance
            try
            {
                $this->databaseHandler = new PDO($dsn);
            }
            catch(PDOException $e)
            {
                $this->err = $e->getMessage();
                print_r($this->err);
            }
        }
        else
        {
            // Determine if we are persistent from the passed parameter
            $this->persist = $config["persist"];

            $dsn = $config["driver"] . ":host=" . $config["host"] . ";dbname=" . $config["database"];

            // Create PDO instance
            try
            {
                $this->databaseHandler = new PDO($dsn, $config["username"], $config["password"], $config["driverOptions"]);
            }
            catch(PDOException $e)
            {
                $this->err = $e->getMessage();
                print_r($this->err);
            }
        }
    }

    /**
     * TODO Refine dsn construction (for instance, find a better way to handle different drivers)
     * DatabaseEngine constructor.
     * @param array $config Associative array of database connection settings
     * @param $internal
     */
    public function addConnection($config, $internal)
    {
        // Construct the database source name
        // If internal is set to true, then this is a SQLLite $configuration, so ignore the other arguments
        if($internal == true)
        {
            $dsn = $config["driver"] . ":" . $config["database"];

            // Create PDO instance
            try
            {
                $this->databaseHandler = new PDO($dsn);
            }
            catch(PDOException $e)
            {
                ErrorHandler::logError($e->getMessage(), error_get_last()["message"]);
                print_r($e->getMessage());
            }
        }
        else
        {
            // Determine if we are persistent from the passed parameter
            $this->persist = $config["persist"];

            $dsn = $config["driver"] . ":host=" . $config["host"] . ";dbname=" . $config["database"];

            // Create PDO instance
            try
            {
                $this->databaseHandler = new PDO($dsn, $config["username"], $config["password"], $config["driverOptions"]);
            }
            catch(PDOException $e)
            {
                $this->err = $e->getMessage();
                print_r($this->err);
            }
        }
    }

    /**
     * Closes the connection explicitly (connection closes implicitly after script/file completion)
     */
    public function closeConnection()
    {
        $this->databaseHandler = null;
    }

    // Allows multiple database transactions
    public function beginTransaction()
    {
        return $this->databaseHandler->beginTransaction();
    }

    // Rollback transaction
    public function cancelTransaction()
    {
        return $this->databaseHandler->rollBack();
    }

    /**
     * Prepare the SQL statement for execution and assigns it to the class-level instance
     * @param string $sql
     * @return \PDOStatement
     */
    public function prepareStatement($sql)
    {
         return $this->databaseHandler->prepare($sql);
    }

    /**
     * Derive the type of a passed value and return the PDO integer constant equivalent
     * TODO Add more types and exception handling
     * @param $value
     * @return int
     */
    public function deriveType($value)
    {
        // Set the datatype
        switch ($value)
        {
            case is_int($value):
                $type = PDO::PARAM_INT;
                break;
            case is_bool($value):
                $type = PDO::PARAM_BOOL;
                break;
            case is_null($value):
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;
        }

        return $type;
    }

    // Return result sets
    public function resultSet($query)
    {
        // TODO Make this dynamic with AT MINIMUM an associative array of configurations
        // Execute the sql statement
        // Prepare the statement for execution
        $this->sqlStatement = $this->prepareStatement($query);
        $this->sqlStatement->execute();
        return $this->sqlStatement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     *  Return scalar sets
     * @param string $table
     * @param array $column
     * @param string $clause
     * @return mixed
     */
    public function singleSet($table, $column, $clause)
    {
        $query = "SELECT ";

        // Append the columns
        foreach ($column as $col)
        {
            $query .= ($col === end($column) ? $col : $col . ", ");
        }

        // Append the source table and where clause
        $query .= " FROM " . $table . " " . $clause . ";";

        // Prepare the statement for execution
        $this->sqlStatement = $this->prepareStatement($query);

        // Execute the sql statement
        $this->sqlStatement->execute();
        return $this->sqlStatement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Inserts a single record to the specified table
     * @param string $table The table name
     * @param $column
     * @param $values
     */
    public function singleInsert($table, $column, $values)
    {
        if(count($column) == count($values)) {
            $sql = "INSERT INTO " . $table . "("; // Start the insert statement
            $valuesClause = " VALUES ("; // Start the value portion of the insert statement
            $bindVariables = []; // Collection for the bind variables

            // Iterate through the columns
            for ($i = 0; $i < count($column); $i++) {
                // Append the column name and a comma unless this is the last element
                $sql .= ($column[$i] === end($column) ? $column[$i] . ")" : $column[$i] . ", ");

                // Convert each column name into a bind variable and store for binding later
                $bindVariables[] = ":" . $column[$i];

                // Convert each column name into a bind variable and append it and a comma unless this is the last element to the values
                $valuesClause .= ($column[$i] === end($column) ? ":" . $column[$i] . ");" : ":" . $column[$i] . ", ");
            }

            // Append the bind variables to the rest of the statement
            $sql .= $valuesClause;

            // Prepare the statement for execution
            $this->sqlStatement = $this->prepareStatement($sql);

            // Bind all of the values
            for ($i = 0; $i < count($values); $i++)
            {
                $this->sqlStatement->bindValue($bindVariables[$i], $values[$i], $this->deriveType($values[$i]));
            }

            // Finally, execute the statement
            $this->sqlStatement->execute();
        }
        else
        {
            echo "<pre>ErrorNav trying to insert into table { $table }: Column count does not match Values</pre>";
        }
    }

    /**
     * TODO This will need heavy refinement to accommodate different database's, etc.
     * WIP This creates a table based off of the passed configurations
     * @param array $config Associative array of configurations
     */
    public function createTable($config)
    {
        $sql = "CREATE TABLE ";
    }

    /**
     * Create a table according to SQLite requirements
     * @param array $config
     */
    public function createSQLiteTable($config)
    {
        // Open the statement
        $statement = "CREATE TABLE IF NOT EXISTS " . $config["table"] . "("; // Only create the table if it doesn't already exist
        $columns = $config["columns"]; // Store the columns in a local variable

        // Append the columns
        for ($i = 0; $i < count($columns); $i++)
        {
            // Append the column name and a comma unless this is the last element
            $statement .= ($columns[$i] === end($columns) ? $columns[$i] . ");" : $columns[$i] . ", ");
        }

        // Prepare the statement for execution
        $this->sqlStatement = $this->prepareStatement($statement);

        // Finally, execute the statement
        $this->sqlStatement->execute();
    }

    /**
     * Return the number of rows affected by the last query
     * @return int
     */
    public function rowCount()
    {
        $this->sqlStatement = $this->prepareStatement($this->sqlStatement);
        $this->sqlStatement->execute();
        return $this->sqlStatement->rowCount();
    }

    // TODO Everything below is non functioning
//    // Use to fetch single column
//    public function returnScalar($index)
//    {
//        $this->execute();
//        $res = $this->sqlStatement->fetchColumn($index);
//        return $res;
//    }
//
//    // Return last id inserted as string
//    public function lastInsertId()
//    {
//        return $this->databaseHandler->lastInsertId();
//    }


//    // Dumps the prepared statements parameters
//    public function debugDumpParams()
//    {
//        return $this->sqlStatement->debugDumpParams();
//    }



//    // Generates a hash of a password
//    function generateHash($password)
//    {
//        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH)
//        {
//            $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
//            return crypt($password, $salt);
//        }
//    }
//
//    // Compares two password hashes
//    function verify($password, $hashedPassword)
//    {
//        return crypt($password, $hashedPassword) == $hashedPassword;
//    }
}