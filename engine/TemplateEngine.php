<?php
/**
 * TODO !WIP! This is currently in the very primitive stages. This will be progressively subject to heavy modification,
 * TODO expansion, and potentially complete transformation.
 *
 * This class converts delimited text into predefined HTML and PHP elements.
 */

namespace util\engine;

class TemplateEngine
{
    protected $templates = [];

    /**
     * TemplateEngine constructor.
     */
    public function __construct()
    {
        //Read all template files into an associative array with the keys being the file names,
        // and the value being the HTML contents. Then they will be available for retrieval
//        $templateFiles = glob("../app/templates/*.html", GLOB_BRACE);
//        foreach($templateFiles as $template)
//        {
//            if (is_readable($template))
//            {
//                $tName = basename($template, '.html');
//                $this->templates += [$tName => html_entity_decode(file_get_contents($template))];
//            }
//            else
//            {
//                echo "Invalid template: { $template }";
//            }
//        }
    }

    /**
     * Primitive way of simply grabbing the html fragment as a file and spitting it's contents out
     * This will need refinement
     * @param string $templateName
     */
    public function injectTemplate($templateName)
    {
        // TODO Replace this with central configuration entry
        $template = "../app/templates/" . $templateName . ".html";
        print_r(html_entity_decode(file_get_contents($template)));
    }

    /**
     * Parses the given file for delimited keywords that are matched against a collection of registered functions
     * that output the corresponding HTML and/or PHP.
     * @param string $template The template file to be parsed
     */
    public function parseTemplate($template)
    {
        echo $this->interpretRawHTML($template);
    }

    /**
     * Interprets the encountered 'raw' HTML and basically injects it into the Template that will eventually be returned
     * @param string $html The HTML to be interpreted
     * @return string The raw HTML section
     */
    private function interpretRawHTML($html)
    {
        return "Raw HTML section: $html";
    }
}