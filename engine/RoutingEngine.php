<?php
	/**
	 *WIP
	 * This handles all of the routing of the application
	 */

	namespace util\engine;

	 class RoutingEngine extends Engine
	 {
         /**
          * Associative array acting as the routing table
		  * @var array
          */
         protected $routes = [];

         /**
          * Components of the route, e.g., controller, action, variable, etc.
		  * @var array
          */
         protected $routeComponents = [];

         /**
          * RoutingEngine constructor.
          */
         public function __construct()
         {
         }

         /**
          * Adds route to the routing table. The $route itself is converted to a regular expression and serves as the key
          * to the associative array of routes.
		  * @param string $route The route URL
		  * @param array $components URL parameters (controllers and actions, etc)
          */
         public function addRoute($route, $components = [])
		 {
             // First, convert the route to a regular expression by escaping the forward slashes
             $route = preg_replace('/\//', '\\/', $route);

             /*
              * Then attempt to convert any possible variable components, e.g., controllers and actions.
              * Find: '{' and 1 or more lower alpha and '}'
              * Replace: Capture matches from the first group into a back-referenced name that is one or more alpha
              */
             $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);

             /*
              * Then attempt to extract any possible variable components containing regular expressions with named capture groups,
              * e.g., {id:\d+} (one or more numbers with the capture group named 'id')
              * Find: '{' and a named capture group that consists of 1 or more lowercase alphabetic characters that represents
              * the value found in the pattern "not 1 or more '}' (to ignore the first delimiter that always encloses the controller) and 1 '}'
              * Replace: Capture matches from the 1st and 2nd variable group (an id or some other session specific variable) into a back-referenced name, e.g., id:
              */
             $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

             // Finally, then return the converted controller, action, etc, enclosed in the starting and ending delimiters suffixed with the case insensitive flag
             $route = '/^' . $route . '$/i';
		 	 $this->routes[$route] = $components; // Them add the route components,
		 }

		 /**
		  * Match the incoming route url to those stored within the routing table, and if a match is true, stores the components
          * of the url, e.g., controller/id/action into the class-level associative array for comparing against valid methods of the
          * controller class later
		  * @param string $queryString
		  * @return boolean True if a match is found, and false otherwise
		  */
		 public function matchRoute($queryString)
		 {
             $queryString = $this->removeQueryStringParams($queryString); // First remove the query string parameters

             // Iterate through the routing table, using the route as the key for the associative array, and value remains unset
             // ( The syntax for iterating over an associative array is: $array as $key => $value)
             foreach ($this->routes as $route => $components)
             {
                 // If matches for the query string is found within the routing table, return them
                 if(preg_match($route, $queryString, $matches))
                 {
                     // Iterate through all of the matches
                     foreach ($matches as $routeKey => $match)
                     {
                         // Ensure the route key is a string
                         if (is_string($routeKey))
                         {
                             $components[$routeKey] = $match; // Set the key and value of the parameter array
                         }
                     }

                     $this->routeComponents = $components; // Set the class-level instance route components to the extracted values so it is available for dispatch
                     return true;
                 }
             }

			 // Default to returning false
			 return false;
		 }

         /**
          * Dispatch the route to the appropriate controller by checking first if the route matches one stored within the
          * routing table. The matchRoute method also extracts and ensures that the route components are stored within the
          * class-level associative array of route components. These components are then validated dependent on their type,
          * e.g., controllers against valid controller classes, and actions against valid methods within those classes.
          * If everything is valid, the class is dynamically instantiated and the method called using the stored variable
          * controller/action values.
          * @param string $queryString The incoming route url
          * @return void
          */
         public function dispatch($queryString)
         {
             // Match the route against the routing table and store the extracted results
             if($this->matchRoute($queryString))
             {
                 $controller = $this->routeComponents['controller']; // Get the route components from the class-level collection
                 $controller = $this->formatCase($controller, 'studly'); // Format the class name to be init cap (studly caps)
                 $controller = $this->getNamespace() . $controller;

                 // Ensure this is a valid class by first checking for its existence
                 if (class_exists($controller))
                 {
                     $controller_object = new $controller($this->routeComponents); // Instantiate the controller, passing in the route parameters
                     $action = $this->routeComponents['action']; // Now that we know this is a valid controller, get the action
                     $action = $this->formatCase($action, 'camel'); // Format the action to be camel case

                     /* Ensure the action is a valid callable method of the controller and is not suffixed with '_action',
                        which would mean an invalid call such as url injection is occurring */
                     if (preg_match("/_action$/i", $action) == 0 && is_callable([$controller_object, $action]))
                     {
                         $controller_object->$action(); // Fulfill the action by calling the verified method
                     }
                     else
                     {
                         $appError = "Dispatch ErrorNav: Inaccessible or invalid method call { $action } for controller class { $controller }";
                         $this->logError($appError);
                         echo $appError;
                     }
                 }
                 else
                 {
                     $appError = "Dispatch ErrorNav: Controller class { $controller } not found";
                     $this->logError($appError);
                     echo $appError;
                 }
             }
             else
             {
                 $appError = "Dispatch ErrorNav: Route { $queryString } not found";
                 $this->logError($appError);
                 echo $appError;
             }
         }

         /**
          * Returns the namespace subdirectory of the controller class
          * @return string The namespace subdirectory of the controller class
          */
         protected function getNamespace()
         {
             $namespace = 'app\controllers\\';

             if(array_key_exists('namespace', $this->routeComponents))
             {
                 $namespace .= $this->routeComponents['namespace'] . "\\";
             }

             return $namespace;
         }

         /**
          * Removes the parameters from the route's query string
          * @param string $queryString The query string
          * @return string The query string sans parameters
          */
         public function removeQueryStringParams($queryString)
         {
             if (!empty($queryString))
             {
                 // Collect the parameters by 'splitting' the query string on the url parameter delimiter '&'
                $params = explode('&', $queryString, 2);
                if(strpos($params[0], '=') === false)
                {
                    $queryString = $params[0];
                }
                else
                {
                    $queryString = '';
                }
             }

             return $queryString;
         }

         /**
          * Returns all routes from the routing table
          * @return array of routes
          */
         public function getRoutes()
         {
             return $this->routes;
         }

		 /**
		  * Returns the matched (valid) route components
		  * @return array Route components
		  */
		 public function getRouteComponents()
		 {
		 	return $this->routeComponents;
		 }

		 /**
          * Formats the incoming string according to the specified format, e.g., camel for camelCase
          * @param string $string The string to format
          * @param string $format The type of format
          * @throws \Exception Custom exception for invalid format types
          * @return string The formatted string
          */
		 public function formatCase($string, $format)
         {
             // First sanitize the string of any hyphens or spaces that are inappropriate for class names, or methods, etc.,
             $formattedString = preg_replace('(-| )', '', $string);

             switch ($format)
             {
                 // Studly Caps (init caps), which is the PHP standard for class names
                 case 'studly':
                     return ucwords($formattedString);
                     break;
                 // Camel Case, which is the PHP standard for methods and functions
                 case 'camel':
                     return lcfirst(ucwords($formattedString));
                     break;
                 /* Constants, ignore the previously sanitized string for other formats, and sanitize the original passed string
                    of spaces in hyphens, replacing them with underscores, and then simply convert to upper case before returning */
                 case 'const':
                     return preg_replace( '/(-| )/', '_', strtoupper($string));
                 // Throw an exception if an invalid format is specified
                 default:
                     $app_error = "URL Format ErrorNav: Invalid format: $format specified.";
                     $this->logError($app_error);
             }
         }

         private function logError($appError)
         {
             parent::getErrorHandler()->logError($appError);
         }
	 }