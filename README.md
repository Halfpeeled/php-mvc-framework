# README #

This PHP MVC framework project was first initialized as an answer to other web frameworks that require too many dependencies, that result in hard to maintain applications and a lot of unnecessary bloat. 
The only dependencies thus far are minimum of PHP 7.1, and an Apache web server.

The main mechanism for routing requests is handled via the Apache rewrite engine and PHP's ability to use dynamic instantiation of class objects via URL parameters extracted using regular expressions with named variables.

The project remains unfinished, including a bare bones templating engine and the beginnings of object relational mapping framework.

